import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const Gap = props => {
  return <View style={{height: props.height, width: props.width}} />;
};

export default Gap;
