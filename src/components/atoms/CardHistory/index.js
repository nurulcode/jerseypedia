import React from 'react';
import {View, Text, StyleSheet, Image, ScrollView} from 'react-native';
import {
  colors,
  fonts,
  numberWithCommas,
  responsiveHeight,
  responsiveWidth,
} from '../../../utils';
import Gap from '../Gap';

const CardHistory = ({pesanan}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.tanggal}>{pesanan.tanggalPemesanan}</Text>
      {pesanan.pesanans.map((history, index) => (
        <View key={index} style={styles.history}>
          <Text style={styles.textBold}>{index + 1} . </Text>
          <Image style={styles.jersey} source={history.product.gambar[0]} />

          <View style={styles.desc}>
            <Text style={styles.nama}>{history.product.nama}</Text>
            <Text style={styles.harga}>
              Rp. {numberWithCommas(history.product.harga)}
            </Text>

            <Gap height={10} />

            <Text style={styles.textBold}>Pesan : {history.jumlahPesan}</Text>
            <Text style={styles.textBold}>
              Total Harga : {numberWithCommas(history.totalHarga)}
            </Text>
          </View>
        </View>
      ))}

      <Gap height={10} />

      <View style={styles.footer}>
        <View style={styles.label}>
          <Text style={styles.textBlue}>Status : </Text>
          <Text style={styles.textBlue}>Ongkit (2-3 Hari) : </Text>
          <Text style={styles.textBlue}>Total Harga : </Text>
        </View>

        <View style={styles.label}>
          <Text style={styles.textBlue}>{pesanan.status}</Text>
          <Text style={styles.textBlue}>Rp. 15.000</Text>
          <Text style={styles.textBlue}>
            Rp. {numberWithCommas(pesanan.totalHarga + 15000)}
          </Text>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    padding: 15,
    borderRadius: 10,
    marginBottom: 20,
  },
  history: {
    flexDirection: 'row',
    marginTop: 10,
  },
  jersey: {
    width: responsiveWidth(66),
    height: responsiveHeight(66),
  },
  tanggal: {
    fontSize: 14,
    fontFamily: fonts.primary.regular,
    color: colors.drak,
  },
  textBold: {
    fontSize: 11,
    fontFamily: fonts.primary.regular,
    color: colors.drak,
  },
  nama: {
    fontSize: 13,
    fontFamily: fonts.primary.regular,
    color: colors.drak,
    textTransform: 'capitalize',
  },
  harga: {
    fontSize: 12,
    fontFamily: fonts.primary.regular,
    color: colors.drak,
  },
  footer: {
    flexDirection: 'row',
  },
  label: {
    flex: 1,
  },
  textBlue: {
    fontSize: 14,
    fontFamily: fonts.primary.regular,
    color: colors.primary,
    textTransform: 'uppercase',
    textAlign: 'right',
  },
});

export default CardHistory;
