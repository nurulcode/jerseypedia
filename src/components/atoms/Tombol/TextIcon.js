import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {IconBack, IconKeranjang, IconKeranjangPutih, IconSubmit} from '../../../assets';
import {colors, fonts} from '../../../utils';
import TextOnly from './TextOnly';

const TextIcon = props => {
  const Icon = () => {
    if (props.icon == 'keranjang') {
      return <IconKeranjang />;
    } else if (props.icon == 'arrow-left') {
      return <IconBack />;
    } else if (props.icon == 'keranjang-putih') {
      return <IconKeranjangPutih />;
    }else if (props.icon == 'submit') {
      return <IconSubmit />;
    }

    return <IconKeranjang />;
  };

  if (props.type == 'text') {
    return <TextOnly {...props} />;
  }

  return (
    <TouchableOpacity style={styles.container(props)} onPress={props.onPress} disabled={props.disabled}>
      <Icon />
      <Text style={styles.title(props.fontSize)} > {props.title}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: props => ({
    padding: props.padding,
    backgroundColor: props.disabled ? colors.border :  colors.primary,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  }),
  title: fontSize => ({
    color: colors.white,
    fontSize: fontSize ? fontSize : 15,
    fontFamily: fonts.primary.regular,
  }),
});

export default TextIcon;
