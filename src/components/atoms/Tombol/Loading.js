import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';

import {colors, fonts} from '../../../utils';

const TombolLoading = props => {
  return (
    <TouchableOpacity style={styles.container(props)}>
      <ActivityIndicator size="small" color="#FFF" />
      <Text style={styles.title(props.fontSize)}>Loading . . .</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: props => ({
    padding: props.padding,
    backgroundColor: colors.border,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  }),
  title: fontSize => ({
    color: colors.white,
    fontSize: fontSize ? fontSize : 15,
    fontFamily: fonts.primary.regular,
  }),
});

export default TombolLoading;
