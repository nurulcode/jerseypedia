import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {IconArrowRight} from '../../../assets';
import {clearStorage, colors, fonts, responsiveHeight} from '../../../utils';
import FIREBASE from '../../../config/FIREBASE';

const CardMenu = ({menu, navigation}) => {
  const onSubmit = () => {
    if (menu.halaman == 'Login') {
      FIREBASE.auth()
        .signOut()
        .then(() => {
          clearStorage();
          navigation.replace('Login');
        })
        .catch(error => {
          alert(error)
        });
    } else {
      navigation.navigate(menu.halaman);
    }
  };

  return (
    <TouchableOpacity
      onPress={() => onSubmit()}
      style={styles.card}>
      <View style={styles.container}>
        <View style={styles.menu}>
          <Text style={styles.text}>{menu.nama}</Text>
        </View>

        <IconArrowRight />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  card: {
    marginTop: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,

    marginHorizontal: 30,
    padding: responsiveHeight(15),
    borderRadius: 10,
    backgroundColor: colors.white,
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  text: {
    fontSize: 16,
    fontFamily: fonts.primary.regular,
    marginLeft: 10,
    color: colors.drak,
  },
  menu: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default CardMenu;
