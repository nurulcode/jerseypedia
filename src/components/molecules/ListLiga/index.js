import React from 'react';
import {View, Text, StyleSheet, ActivityIndicator} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {colors} from '../../../utils';
import CardLiga from '../../atoms/CardLiga';

const ListLiga = props => {
  const data = useSelector(state => state.LigaReducer);
  const {ligaLoading, ligaData, ligaError} = data;

  return (
    <View style={styles.container}>
      {ligaData ? (
        Object.keys(ligaData).map(key => (
          <CardLiga liga={ligaData[key]} key={key} id={key}/>
        ))
      ) : ligaLoading ? (
        <View style={styles.loading}>
          <ActivityIndicator color={colors.primary} />
        </View>
      ) : (
        <Text>List Data Kosong</Text>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  loading: {
    flex: 1,
    marginTop: 10,
    marginBottom: 30,
  },
});

export default ListLiga;
