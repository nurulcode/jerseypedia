import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import {colors} from '../../../utils';
import {CardKeranjang} from '../../atoms';

const ListKeranjang = data => {
  const {listKeranjangData, listKeranjangLoading, listKeranjangError} = data;

  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.container}>
        {listKeranjangData ? (
          Object.keys(listKeranjangData.pesanans).map(key => (
            <CardKeranjang
              keranjang={listKeranjangData.pesanans[key]}
              keranjangUtama={listKeranjangData}
              key={key}
              id={key}
            />
          ))
        ) : listKeranjangLoading ? (
          <View style={styles.loading}>
            <ActivityIndicator color={colors.primary} />
          </View>
        ) : (
          <View>
            <Text style={styles.text}>List Data Kosong</Text>
          </View>
        )}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    marginVertical: 10,
  },
  loading: {
    flex: 1,
    marginTop: 10,
    marginBottom: 30,
  },
  text: {
    textAlign: 'center',
    marginTop: 10,
    fontSize: 18
  },
});

export default ListKeranjang;
