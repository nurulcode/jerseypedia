import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import CardMenu from '../../atoms/CardMenu';


const ListMenu = ({menus, navigation}) => {
  return (
    <View>
      {menus.map(menu => (
        <CardMenu key={menu.id} menu={menu} navigation={navigation}/>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
    container: {},
  });
  
  
export default ListMenu;
