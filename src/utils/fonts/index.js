export const fonts = {
  primary: {
    light: 'PublicSans-Light',
    regular: 'PublicSans-Regular',
    semibold: 'PublicSans-PublicSans-SemiBold',
    bold: 'PublicSans-Bold',
  },
};
