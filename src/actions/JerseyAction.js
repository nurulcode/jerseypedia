import FIREBASE from '../config/FIREBASE';
import {
  dispatchError,
  dispatchLoading,
  dispatchSuccess,
  storeData,
} from '../utils';

import {
  JERSEY_BY_LIGA_LIST,
  JERSEY_BY_LIGA_REMOVE,
  JERSEY_ERROR,
  JERSEY_LIMITE_ERROR,
  JERSEY_LIMITE_LOADING,
  JERSEY_LIMITE_SUCCESS,
  JERSEY_LOADING,
  JERSEY_SUCCESS,
  SEARCH_JERSEY,
} from '../constants/JerseyConstat';

export const getListJersey = (idLiga, keyword) => {
  return dispatch => {
    dispatchLoading(dispatch, JERSEY_LOADING);

    if (idLiga) {
      FIREBASE.database()
        .ref('jerseys')
        .orderByChild('liga')
        .equalTo(idLiga)
        .once('value')
        .then(res => {
          dispatchSuccess(dispatch, JERSEY_SUCCESS, res.val() ? res.val() : []);
          // storeData('user', dataBaru);
        })
        .catch(error => {
          dispatchError(dispatch, JERSEY_ERROR, error.message);
          alert(error.message);
        });
    } else if (keyword) {
      FIREBASE.database()
        .ref('jerseys')
        .orderByChild('klub')
        .equalTo(keyword.toUpperCase())
        .once('value')
        .then(res => {
          dispatchSuccess(dispatch, JERSEY_SUCCESS, res.val() ? res.val() : []);
          // storeData('user', dataBaru);
        })
        .catch(error => {
          dispatchError(dispatch, JERSEY_ERROR, error.message);
          alert(error.message);
        });
    } else {
      FIREBASE.database()
        .ref('jerseys')
        .once('value')
        .then(res => {
          dispatchSuccess(dispatch, JERSEY_SUCCESS, res.val() ? res.val() : []);
          // storeData('user', dataBaru);
        })
        .catch(error => {
          dispatchError(dispatch, JERSEY_ERROR, error.message);
          alert(error.message);
        });
    }
  };
};

export const getLimitJersey = () => {
  return dispatch => {
    dispatchLoading(dispatch, JERSEY_LIMITE_LOADING);

    FIREBASE.database()
      .ref('jerseys')
      .limitToLast(4)
      .once('value', querySnapshot => {
        let data = querySnapshot.val();

        dispatchSuccess(dispatch, JERSEY_LIMITE_SUCCESS, data);
      })
      .catch(error => {
        dispatchError(dispatch, JERSEY_LIMITE_ERROR, error);
        alert(error);
      });
  };
};

export const getJerseyByLiga = (idLiga, namaLiga) => ({
  type: JERSEY_BY_LIGA_LIST,
  payload: {
    idLiga,
    namaLiga,
  },
});

export const removeJerseyByLiga = () => ({
  type: JERSEY_BY_LIGA_REMOVE,
});

export const saveKeywordJersey = search => ({
  type: SEARCH_JERSEY,
  payload: {
    data: search,
  },
});
