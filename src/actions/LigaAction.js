import FIREBASE from '../config/FIREBASE';
import {
  LIGA_DETAIL_ERROR,
  LIGA_DETAIL_LOADING,
  LIGA_DETAIL_SUCCESS,
  LIGA_ERROR,
  LIGA_LOADING,
  LIGA_SUCCESS,
} from '../constants/LigaConstat';
import {
  dispatchError,
  dispatchLoading,
  dispatchSuccess,
  storeData,
} from '../utils';

export const getListLiga = data => {
  return dispatch => {
    dispatchLoading(dispatch, LIGA_LOADING);

    FIREBASE.database()
      .ref('ligas')
      .once('value')
      .then(res => {
        dispatchSuccess(dispatch, LIGA_SUCCESS, res.val() ? res.val() : []);
      })
      .catch(error => {
        dispatchError(dispatch, LIGA_ERROR, error.message);
        alert(error.message);
      });
  };
};

export const getLigaById = id => {
  return dispatch => {
    dispatchLoading(dispatch, LIGA_DETAIL_LOADING);
    FIREBASE.database()
      .ref('ligas/' + id)
      .once('value')
      .then(res => {
        dispatchSuccess(
          dispatch,
          LIGA_DETAIL_SUCCESS,
          res.val() ? res.val() : [],
        );
      })
      .catch(error => {
        dispatchError(dispatch, LIGA_DETAIL_ERROR, error.message);
        alert(error.message);
      });
  };
};
