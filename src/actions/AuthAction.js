import FIREBASE from '../config/FIREBASE';
import {
  LOGIN_USER_ERROR,
  LOGIN_USER_LOADING,
  LOGIN_USER_SUCCESS,
  REGISTER_USER_ERROR,
  REGISTER_USER_LOADING,
  REGISTER_USER_SUCCESS,
} from '../constants/AuthConstat';

import {
  dispatchError,
  dispatchLoading,
  dispatchSuccess,
  storeData,
} from '../utils';

export const registerUser = (data, password) => {
  return dispatch => {
    dispatchLoading(dispatch, REGISTER_USER_LOADING);

    FIREBASE.auth()
      .createUserWithEmailAndPassword(data.email, password)
      .then(userCredential => {
        // Signed in
        var user = userCredential.user;

        const dataBaru = {
          ...data,
          uid: user.uid,
        };

        FIREBASE.database()
          .ref('users/' + user.uid)
          .set(dataBaru);

        storeData('user', dataBaru);
        dispatchSuccess(dispatch, REGISTER_USER_SUCCESS, dataBaru);
      })

      .catch(error => {
        var errorMessage = error.message;
        dispatchError(dispatch, REGISTER_USER_ERROR, errorMessage);
      });
  };
};

export const loginUser = (email, password) => {
  return dispatch => {
    dispatchLoading(dispatch, LOGIN_USER_LOADING);

    FIREBASE.auth()
      .signInWithEmailAndPassword(email, password)
      .then(userCredential => {
        // Signed in
        var user = userCredential.user;
        FIREBASE.database()
          .ref('/users/' + user.uid)
          .once('value')
          .then(response => {
            if (response) {
              storeData('user', response.val());
              dispatchSuccess(dispatch, LOGIN_USER_SUCCESS, response.val());
            } else {
              dispatchError(dispatch, LOGIN_USER_ERROR, 'Data user tidak ada');
              alert('Data user tidak ada!');
            }
          });
      })
      .catch(error => {
        var errorCode = error.code;
        var errorMessage = error.message;
        dispatchError(dispatch, LOGIN_USER_ERROR, errorMessage);
        alert(errorMessage);
      });
  };
};
