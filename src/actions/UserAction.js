import {GET_USER} from '../constants/UserConstat';

export const getUser = (nama, email) => {
  return dispatch => {
    dispatch({
      type: GET_USER,
      payload: {
        nama,
        email,
      },
    });
  };
};
