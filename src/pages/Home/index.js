import React, {useEffect, useState} from 'react';
import {View, StyleSheet, SafeAreaView, Text, ScrollView} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {getLimitJersey, getListJersey} from '../../actions/JerseyAction';
import {getListLiga} from '../../actions/LigaAction';
import {Gap, Tombol} from '../../components/atoms';
import {
  BannerSlider,
  HeaderConponent,
  ListJerseys,
  ListLiga,
} from '../../components/molecules';
import {fonts} from '../../utils';
import {colors} from '../../utils/colors';

const Home = ({navigation}) => {
  const [show, setShow] = useState(true);
  const dispatch = useDispatch();

  const showAll = () => {
    dispatch(getListJersey());
    setShow(false)
  };

  useEffect(() => {
    dispatch(getListLiga());

    const unsubscribe = navigation.addListener('focus', () => {
      dispatch(getLimitJersey());
      setShow(true)
    });
    return unsubscribe;
  }, [navigation]);

  return (
    <View style={styles.page}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <HeaderConponent navigation={navigation} page="Home"/>
        <BannerSlider />
        <View style={styles.pilihLiga}>
          <Text style={styles.label}>Pilih Liga</Text>
          <Gap height={10} />
          <ListLiga />
        </View>

        <View style={styles.pilihJersey}>
          <Text style={styles.label}>
            Pilih <Text style={styles.labelBold}>Jersey</Text> Yang Anda
            Inginkan
          </Text>
          <Gap height={10} />
          <ListJerseys />

          {show && (
            <Tombol
              type="text"
              title="Lihat Semua"
              padding={7}
              onPress={() => showAll()}
            />
          )}

        </View>
        <Gap height={100} />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: colors.white,
  },
  pilihLiga: {
    marginHorizontal: 30,
    marginTop: 10,
  },
  pilihJersey: {
    marginHorizontal: 30,
    marginTop: 10,
  },
  label: {
    fontSize: 16,
    fontFamily: fonts.primary.regular,
    color: colors.drak,
  },
  labelBold: {
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default Home;
