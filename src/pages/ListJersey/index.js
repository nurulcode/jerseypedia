import React, {useEffect, useState} from 'react';
import {View, StyleSheet, SafeAreaView, Text, ScrollView} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {getListJersey} from '../../actions/JerseyAction';
import {getListLiga} from '../../actions/LigaAction';
import {Gap, Tombol} from '../../components/atoms';
import {
  HeaderConponent,
  ListJerseys,
  ListLiga,
} from '../../components/molecules';
import {fonts} from '../../utils';
import {colors} from '../../utils/colors';

const ListJersey = ({navigation}) => {
  const dispatch = useDispatch();

  const data = useSelector(state => state.JerseyByLigaReducer);
  const {idLiga, namaLiga, keyword} = data;

  useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      dispatch(getListLiga());
      dispatch(getListJersey(idLiga, keyword));
    });
    return unsubscribe;
  }, [navigation]);

  useEffect(() => {
    dispatch(getListJersey(idLiga, keyword));
  }, [idLiga, keyword]);

  return (
    <View style={styles.page}>
      <HeaderConponent page="ListJersey" />
      <ScrollView showsVerticalScrollIndicator={false} style={styles.container}>
        <View style={styles.pilihLiga}>
          <ListLiga />
        </View>

        <View style={styles.pilihJersey}>
          {keyword ? (
            <Text style={styles.label}>
              Cari <Text style={styles.labelBold}>{keyword}</Text>
            </Text>
          ) : (
            <Text style={styles.label}>
              Pilih <Text style={styles.labelBold}>Jersey</Text>
              {namaLiga ? namaLiga : ' Yang Anda Inginkan'}
            </Text>
          )}
          <Gap height={10} />
          <ListJerseys />
        </View>
        <Gap height={80} />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: -25,
  },
  page: {
    flex: 1,
    backgroundColor: colors.white,
  },
  pilihLiga: {
    marginHorizontal: 30,
  },
  pilihJersey: {
    marginHorizontal: 30,
    marginTop: 10,
  },
  label: {
    fontSize: 16,
    fontFamily: fonts.primary.regular,
    color: colors.drak,
  },
  labelBold: {
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default ListJersey;
