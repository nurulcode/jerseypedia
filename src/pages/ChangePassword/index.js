import React from 'react';
import {View, StyleSheet, ScrollView, Image} from 'react-native';
import {Inputan, Tombol} from '../../components/atoms';
import {colors, fonts, responsiveHeight, responsiveWidth} from '../../utils';

const ChangePassword = () => {
  return (
    <View style={styles.pages}>
      <View>
        <Inputan label="Password Lama" secureTextEntry />
        <Inputan label="Password Baru" secureTextEntry />
        <Inputan label="Konfirmasi Password Baru" secureTextEntry />
      </View>

      <View style={styles.submit}>
        <Tombol
          title="Ubah Password"
          type="textIcon"
          icon="submit"
          padding={responsiveHeight(15)}
          fontSize={18}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  pages: {
    flex: 1,
    paddingHorizontal: 30,
    backgroundColor: colors.white,
    paddingTop: 10,
    justifyContent: 'space-between'
  },
  label: {
    fontSize: 18,
    fontFamily: fonts.primary.regular,
    color: colors.drak,
  },
  submit: {
    marginVertical: 10,
  },
});

export default ChangePassword;
