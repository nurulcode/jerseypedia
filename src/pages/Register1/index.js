import React from 'react';
import {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  ScrollView,
  Keyboard,
  KeyboardAvoidingView,
  Alert,
} from 'react-native';

import {IlustrasiRegister1} from '../../assets';
import {Gap, Inputan, Tombol} from '../../components/atoms';
import {colors, fonts, responsiveHeight, responsiveWidth} from '../../utils';

const Register1 = ({navigation}) => {
  const [nama, setNama] = useState('');
  const [email, setEmail] = useState('');
  const [nohp, setNohp] = useState('');
  const [password, setPassword] = useState('');

  onContinue = () => {
    if (nama && email && nohp && password) {
      navigation.navigate('Register2', {
        nama,
        email,
        nohp,
        password,
      });
    } else {
      Alert.alert('Error', 'Nama, Email, NoHp dan Password tidak boleh kosong');
    }
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.pages}>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <ScrollView>
          <View>
            <View style={styles.btnBack}>
              <Tombol icon="arrow-left" onPress={() => navigation.goBack()} />
            </View>
            <View style={styles.ilustrasi}>
              <IlustrasiRegister1 />
              <Gap height={5} />
              <Text style={styles.title}>Daftar</Text>
              <Text style={styles.title}>Isi Daftar Diri Anda</Text>

              <View style={styles.wrapperCircle}>
                <View style={styles.circlePrimary}></View>
                <Gap width={10} />
                <View style={styles.circleDisabled}></View>
              </View>
            </View>

            <View style={styles.card}>
              <Inputan
                label="Nama"
                value={nama}
                onChangeText={nama => setNama(nama)}
              />
              <Inputan
                label="E-Mail"
                value={email}
                onChangeText={email => setEmail(email)}
              />
              <Inputan
                label="No. Handphone"
                keyboardType="number-pad"
                value={nohp}
                onChangeText={nohp => setNohp(nohp)}
              />
              <Inputan
                label="password"
                secureTextEntry
                value={password}
                onChangeText={password => setPassword(password)}
              />
              <Gap height={25} />

              <Tombol
                title="Continue"
                type="textIcon"
                icon="submit"
                padding={10}
                fontSize={18}
                onPress={() => onContinue()}
              />
            </View>
          </View>
        </ScrollView>
      </TouchableWithoutFeedback>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  pages: {
    flex: 1,
    backgroundColor: colors.white,
    paddingTop: 20,
  },
  ilustrasi: {
    alignItems: 'center',
  },

  title: {
    fontSize: 24,
    fontFamily: fonts.primary.light,
    color: colors.primary,
  },
  wrapperCircle: {
    flexDirection: 'row',
    marginTop: 10,
  },
  circlePrimary: {
    backgroundColor: colors.primary,
    width: responsiveWidth(11),
    height: responsiveWidth(11),
    borderRadius: 10,
  },
  circleDisabled: {
    backgroundColor: colors.border,
    width: responsiveWidth(11),
    height: responsiveWidth(11),
    borderRadius: 10,
  },

  card: {
    backgroundColor: colors.white,
    marginHorizontal: 30,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
    paddingHorizontal: 30,
    paddingBottom: 20,
    paddingTop: 10,
    borderRadius: 10,
    marginTop: 10,
    marginBottom: 10,
  },
  btnBack: {
    marginLeft: 30,
    position: 'absolute',
  },
});

export default Register1;
