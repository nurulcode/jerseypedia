import React, {useState} from 'react';
import {View, Text, StyleSheet, ScrollView} from 'react-native';
import {ListHistory} from '../../components/molecules';
import {dummyPesanans} from '../../data';
import {colors} from '../../utils';

const History = () => {
  const [pesanans, setPesanans] = useState(dummyPesanans);

  return (
    <View style={styles.pages}>
      <ScrollView>
        <ListHistory pesanans={pesanans} />
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  pages: {
    backgroundColor: colors.white,
    flex: 1,
  },
});

export default History;
