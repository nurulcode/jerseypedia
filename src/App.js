import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import Router from './router';
import {Provider} from 'react-redux';
import store from './store';

function App() {
  return (
    // initialRouteName = router yang pertama kali di jalankan
    <Provider store={store}>
      <NavigationContainer initialRouteName="Splash">
        <Router />
      </NavigationContainer>
    </Provider>
  );
}

export default App;
