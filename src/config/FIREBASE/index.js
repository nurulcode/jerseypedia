import firebase from 'firebase';

firebase.initializeApp({
  apiKey: 'AIzaSyDfWD1ECgu_BgLgyRjKxGVW2sg-lRllZIE',
  authDomain: 'jersypedia-11895.firebaseapp.com',
  projectId: 'jersypedia-11895',
  storageBucket: 'jersypedia-11895.appspot.com',
  messagingSenderId: '487336743402',
  appId: '1:487336743402:web:f5a9d3df53e61eb6d26293',
  databaseURL: 'https://jersypedia-11895-default-rtdb.asia-southeast1.firebasedatabase.app'
});

const FIREBASE = firebase;

export default FIREBASE;
