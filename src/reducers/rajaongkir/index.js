import {
  GET_KOTA,
  GET_KOTA_DETAIL,
  GET_PROVINSI,
} from '../../constants/RajaOngkirConstat';

const initialState = {
  provinsiLoading: false,
  provinsiData: false,
  provinsiError: false,

  kotaLoading: false,
  kotaData: false,
  kotaError: false,

  kotaDetailLoading: false,
  kotaDetailData: false,
  kotaDetailError: false,
};

export const RajaOngkirReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_PROVINSI:
      return {
        ...state,
        provinsiLoading: action.payload.loading,
        provinsiData: action.payload.data,
        provinsiError: action.payload.errorMessage,
      };
    case GET_KOTA:
      return {
        ...state,
        kotaLoading: action.payload.loading,
        kotaData: action.payload.data,
        kotaError: action.payload.errorMessage,
      };
    case GET_KOTA_DETAIL:
      return {
        ...state,
        kotaDetailLoading: action.payload.loading,
        kotaDetailData: action.payload.data,
        kotaDetailError: action.payload.errorMessage,
      };
    default:
      return initialState;
  }
};
