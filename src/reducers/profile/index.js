import { UPDATE_PROFILE } from "../../constants/UserConstat";

const initialState = {
  updateProfileLoading: false,
  updateProfileData: false,
  updateProfileError: false,
};

export const ProfileReducer = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_PROFILE:
      return {
        ...state,
        updateProfileLoading: action.payload.loading,
        updateProfileData: action.payload.data,
        updateProfileError: action.payload.errorMessage,
      };
    default:
      return initialState;
  }
};