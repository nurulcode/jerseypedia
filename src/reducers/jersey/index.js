import {
  JERSEY_BY_LIGA_LIST,
  JERSEY_ERROR,
  JERSEY_LIMITE_ERROR,
  JERSEY_LIMITE_LOADING,
  JERSEY_LIMITE_SUCCESS,
  JERSEY_LOADING,
  JERSEY_SUCCESS,
  JERSEY_BY_LIGA_REMOVE,
  SEARCH_JERSEY
} from '../../constants/JerseyConstat';

const initialState = {
  jerseyLoading: false,
  jerseyData: false,
  jerseyError: false,
};

export const JerseyReducer = (state = initialState, action) => {
  switch (action.type) {
    case JERSEY_LOADING:
      return {
        jerseyLoading: true,
        jerseyData: false,
        jerseyError: false,
      };
    case JERSEY_SUCCESS:
      return {
        ...state,
        jerseyLoading: false,
        jerseyData: action.payload.data,
        jerseyError: false,
      };
    case JERSEY_ERROR:
      return {
        jerseyLoading: false,
        jerseyData: false,
        jerseyError: action.payload.errorMessage,
      };
    case JERSEY_LIMITE_LOADING:
      return {
        ...state,
        jerseyLoading: true,
        jerseyData: false,
        jerseyError: false,
      };
    case JERSEY_LIMITE_SUCCESS:
      return {
        jerseyLoading: false,
        jerseyData: action.payload.data,
        jerseyError: false,
      };
    case JERSEY_LIMITE_ERROR:
      return {
        jerseyLoading: false,
        jerseyData: false,
        jerseyError: action.payload.errorMessage,
      };
    case JERSEY_BY_LIGA_LIST:
      return {
        ...state,
        idLiga: action.payload.idLiga,
        namaLiga: action.payload.namaLiga,
      };
    default:
      return state;
  }
};

export const JerseyByLigaReducer = (
  state = {idLiga: false, namaLiga: false, keyword : false},
  action,
) => {
  switch (action.type) {
    case JERSEY_BY_LIGA_LIST:
      return {
        idLiga: action.payload.idLiga,
        namaLiga: action.payload.namaLiga,
        keyword: false
      };
    case JERSEY_BY_LIGA_REMOVE:
      return {
        idLiga: false,
        namaLiga: false,
        keyword : false
      };
    case SEARCH_JERSEY:
      return {
        idLiga: false,
        namaLiga: false,
        keyword : action.payload.data
      };
    default:
      return state;
  }
};
