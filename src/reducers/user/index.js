import { GET_USER } from "../../constants/UserConstat";

export const UserReducer = (state = {dataUser: false}, action) => {
  switch (action.type) {
    case GET_USER:
      return {
        user: action.payload,
      };

    default:
      return state;
  }
};

// const initialState = {
//   dataUser: false,
// };

// export default function (state = initialState, action) {
//   switch (action.type) {
//     case GET_USER:
//       return {
//         ...state,
//         dataUser: action.payload,
//       };
//     default:
//       return false;
//   }
// }
