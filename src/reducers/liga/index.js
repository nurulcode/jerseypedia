import {GET_LIST_LIGA} from '../../actions/LigaAction';
import {
  LIGA_DETAIL_ERROR,
  LIGA_DETAIL_LOADING,
  LIGA_DETAIL_SUCCESS,
  LIGA_ERROR,
  LIGA_LOADING,
  LIGA_SUCCESS,
} from '../../constants/LigaConstat';

const initialState = {
  ligaLoading: false,
  ligaData: false,
  ligaError: false,
};

export const LigaReducer = (state = initialState, action) => {
  switch (action.type) {
    case LIGA_LOADING:
      return {
        ligaLoading: true,
        ligaData: false,
        ligaError: false,
      };
    case LIGA_SUCCESS:
      return {
        ligaLoading: false,
        ligaData: action.payload.data,
        ligaError: false,
      };
    case LIGA_ERROR:
      return {
        ligaLoading: false,
        ligaData: false,
        ligaError: action.payload.errorMessage,
      };
    default:
      return state;
  }
};

export const LigaByIdReducer = (
  state = {
    ligaDetailLoading: false,
    ligaDetailData: false,
    ligaDetailError: false,
  },
  action,
) => {
  switch (action.type) {
    case LIGA_DETAIL_LOADING:
      return {
        ligaDetailLoading: true,
        ligaDetailData: false,
        ligaDetailError: false,
      };
    case LIGA_DETAIL_SUCCESS:
      return {
        ligaDetailLoading: false,
        ligaDetailData: action.payload.data,
        ligaDetailError: false,
      };
    case LIGA_DETAIL_ERROR:
      return {
        ligaDetailLoading: false,
        ligaDetailData: false,
        ligaDetailError: action.payload.errorMessage,
      };
    default:
      return state;
  }
};
