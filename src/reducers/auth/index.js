import {
  LOGIN_USER_SUCCESS,
  LOGIN_USER_LOADING,
  LOGIN_USER_ERROR,
  REGISTER_USER_LOADING,
  REGISTER_USER_SUCCESS,
  REGISTER_USER_ERROR,
} from '../../constants/AuthConstat';

export const AuthReducer = (
  state = {registerLoading: false, registerData: false, registerError: false},
  action,
) => {
  switch (action.type) {
    case REGISTER_USER_LOADING:
      return {
        registerLoading: action.payload.loading,
        registerData: action.payload.data,
        registerError: action.payload.errorMessage,
      };
    case REGISTER_USER_SUCCESS:
      return {
        registerLoading: action.payload.loading,
        registerData: action.payload.data,
        registerError: action.payload.errorMessage,
      };
    case REGISTER_USER_ERROR:
      return {
        registerLoading: action.payload.loading,
        registerData: action.payload.data,
        registerError: action.payload.errorMessage,
      };
    default:
      return state;
  }
};

export const LoginReducer = (
  state = {loginLoading: false, loginData: false, loginError: false},
  action,
) => {
  switch (action.type) {
    case LOGIN_USER_LOADING:
      return {
        loginLoading: true,
        loginData: false,
        loginError: false,
      };
    case LOGIN_USER_SUCCESS:
      return {
        loginLoading: false,
        loginData: action.payload.data,
        loginError: false,
      };
    case LOGIN_USER_ERROR:
      return {
        loginLoading: false,
        loginData: false,
        loginError: action.payload.errorMessage,
      };
    default:
      return state;
  }
};
