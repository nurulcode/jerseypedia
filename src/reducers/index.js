import {combineReducers} from 'redux';
import {UserReducer} from './user';
import {RajaOngkirReducer} from './rajaongkir';
import {AuthReducer, LoginReducer} from './auth';
import {ProfileReducer} from './profile';
import { LigaReducer, LigaByIdReducer } from './liga';
import { JerseyReducer, JerseyByLigaReducer } from './jersey';
import { KeranjangReducer, ListKeranjangReducer, removeKeranjangReducer } from './keranjang';

const rootReducer = combineReducers({
  UserReducer,
  RajaOngkirReducer,
  AuthReducer,
  LoginReducer,
  ProfileReducer,
  LigaReducer,
  JerseyReducer,
  JerseyByLigaReducer,
  LigaByIdReducer,
  KeranjangReducer,
  ListKeranjangReducer,
  removeKeranjangReducer,
});

export default rootReducer;
